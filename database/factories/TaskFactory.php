<?php

namespace Database\Factories;

use App\Models\Task;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class TaskFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Task::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'task_name' => $this->faker->sentence,
            'description' => $this->faker->paragraph,
            'creator' => User::query()->where('title', 'manager')
                ->inRandomOrder()->first()->id,
            'assigned_to' => User::query()->where('title', 'developer')
                ->inRandomOrder()->first()->id,
            'reported_to' => User::query()->where('title', 'senior developer')
                ->inRandomOrder()->first()->id,

        ];
    }
}
