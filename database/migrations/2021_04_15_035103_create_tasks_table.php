<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('task_name')->nullable();
            $table->longText('description')->nullable();
            $table->unsignedBigInteger('creator');
            $table->unsignedBigInteger('assigned_to');
            $table->unsignedBigInteger('reported_to');
            $table->boolean('is_done')->default(false);
            $table->timestamps();


            $table->foreign('creator')->on('users')
                ->references('id')->cascadeOnDelete();

            $table->foreign('assigned_to')->on('users')
                ->references('id')->cascadeOnDelete();

            $table->foreign('reported_to')->on('users')
                ->references('id')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
