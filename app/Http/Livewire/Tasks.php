<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Task;
use Livewire\WithPagination;

class Tasks extends Component
{
    use WithPagination;

    public function allTasks()
    {
        return Task::query()->get();
    }

    public function render()
    {
        return view('livewire.task',[
            'posts' => $this->allTasks()
            ]);
    }
}
