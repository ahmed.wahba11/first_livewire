<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [

    ];

    public function creator()
    {
        return $this->belongsTo(User::class,'creator');
    }

    public function reportedTo()
    {
        return $this->belongsTo(User::class,'reported_to');
    }

    public function assignedTo()
    {
        return $this->belongsTo(User::class,'assigned_to');
    }

}
