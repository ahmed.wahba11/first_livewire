<div>
    <table class="table-auto">
        <thead>
        <tr>
            <th>id</th>
            <th>task name</th>
            <th>description</th>
            <th>creator</th>
            <th>assigned_to</th>
            <th>reported_to</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            @forelse($posts as $post)
                <td>{{$post->id}}</td>
                <td>{{$post->task_name}}</td>
                <td>{{$post->description}}</td>
                <td>{{$post->creator}}</td>
                <td>{{$post->assigned_to}}</td>
                <td>{{$post->reported_to}}</td>
            @empty
                <p>No Data</p>
            @endforelse
        </tr>

        </tbody>
    </table>
</div>
